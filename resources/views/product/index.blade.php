@extends('layouts.app')
@section('title', $viewData["title"])
@section('subtitle', $viewData["subtitle"])
@section('content')
<div class="row">

    <form action="{{route('product.filter')}}" method="post">
    @csrf
    <label for="filter">Filter By Category:</label>
    <br>
    <select name="category" id="category">
        @foreach ($viewData['categories'] as $category)
        <option {{$category == $viewData['category']?'selected':''}} value="{{$category->getId()}}">{{$category->getName()}}</option>
        @endforeach
    </select>
    <button>Filter</button>
    </form>
  @foreach ($viewData["products"] as $product)
  <div class="col-md-4 col-lg-3 mb-2">
    <div class="card">
      <img src="{{ asset('/storage/'.$product->getImage()) }}" class="card-img-top img-card">
      <div class="card-body text-center">
        <a href="{{ route('product.show', ['id'=> $product->getId()]) }}"
          class="btn bg-primary text-white">{{ $product->getName() }}</a>
      </div>
    </div>
  </div>
  @endforeach
</div>
@endsection
